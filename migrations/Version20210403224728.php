<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210403224728 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE saved_comments (user_id BIGINT NOT NULL, comment_id BIGINT NOT NULL, PRIMARY KEY(user_id, comment_id))');
        $this->addSql('CREATE INDEX IDX_317F9102A76ED395 ON saved_comments (user_id)');
        $this->addSql('CREATE INDEX IDX_317F9102F8697D13 ON saved_comments (comment_id)');
        $this->addSql('CREATE TABLE saved_submissions (user_id BIGINT NOT NULL, submission_id BIGINT NOT NULL, PRIMARY KEY(user_id, submission_id))');
        $this->addSql('CREATE INDEX IDX_91733AE8A76ED395 ON saved_submissions (user_id)');
        $this->addSql('CREATE INDEX IDX_91733AE8E1FD4933 ON saved_submissions (submission_id)');
        $this->addSql('ALTER TABLE saved_comments ADD CONSTRAINT FK_317F9102A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE saved_comments ADD CONSTRAINT FK_317F9102F8697D13 FOREIGN KEY (comment_id) REFERENCES comments (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE saved_submissions ADD CONSTRAINT FK_91733AE8A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE saved_submissions ADD CONSTRAINT FK_91733AE8E1FD4933 FOREIGN KEY (submission_id) REFERENCES submissions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users ALTER submission_link_destination DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE saved_comments');
        $this->addSql('DROP TABLE saved_submissions');
        $this->addSql('ALTER TABLE users ALTER submission_link_destination SET DEFAULT \'url\'');
    }
}
